import { Component, OnInit } from '@angular/core';

import { AuthService } from '../_service/auth.service';
import { TokenStorageService } from '../_service/token-storage.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  roles: string[] = [];

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService, private router: Router ) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
  }

  onSubmit(): void {
    this.authService.login(this.form).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.showDashboardPage();
      },
      err => {
        this.isLoginFailed = true;
      }
    );
  }

  showDashboardPage(): void {
    this.router.navigate(['/home/dashboard']);
  }

  showRegisterPage(): void {
    this.router.navigate(['/register']);
  }
}
