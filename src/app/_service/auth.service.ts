import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {TokenStorageService} from './token-storage.service';
import {Router} from '@angular/router';

const AUTH_API = 'http://localhost:8080/home-purchase-api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService) { }

  login(credentials): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  register(user): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      lastname: user.lastname,
      firstname: user.firstname,
      username: user.username,
      password: user.password
    }, httpOptions);
  }
  isAuthenticated(): boolean {
    return !(this.tokenStorageService.getToken() === null || this.tokenStorageService.getToken() === '');
  }
}
